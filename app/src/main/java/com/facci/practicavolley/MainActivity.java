package com.facci.practicavolley;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity  {

    Button btnJsonObjectActivity, btnJsonArrayActivity, btnArraySinNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnJsonObjectActivity = (Button) findViewById(R.id.btnJsonObjectActivity);
        btnJsonArrayActivity = (Button) findViewById(R.id.btnJsonArrayActivity);
        btnArraySinNombre = (Button) findViewById(R.id.btnArraySinNombre);

        btnJsonObjectActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, JsonObjectActivity .class);
                startActivity(intent);
            }
        });
        btnJsonArrayActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, JsonArrayActivity .class);
                startActivity(intent);
            }
        });

        btnArraySinNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ArraySinNombreActivity.class);
                startActivity(intent);
            }
        });
    }

}
