package com.facci.practicavolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facci.practicavolley.Adaptador.AdaptadorPersonajes;
import com.facci.practicavolley.Modelo.Personajes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonArrayActivity extends AppCompatActivity
        implements Response.Listener<JSONObject>, Response.ErrorListener {

    RecyclerView rvPersonajes;
    ArrayList<Personajes> listaPersonajes;

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_array);

        rvPersonajes = (RecyclerView) findViewById(R.id.recycler);
        listaPersonajes=new ArrayList<>();

        rvPersonajes.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));
        rvPersonajes.setHasFixedSize(true);

        request = Volley.newRequestQueue(getApplicationContext());

        cargarApi();


    }

    private void cargarApi() {
        String url = "https://rickandmortyapi.com/api/character/";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this,this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onResponse(JSONObject response) {

        JSONArray jsonArray = response.optJSONArray("results");

        try {

            for (int i=0; i<jsonArray.length(); i++){
                Personajes personajes = new Personajes();
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                personajes.setName(jsonObject.getString("name"));
                personajes.setGender(jsonObject.getString("gender"));
                personajes.setImage(jsonObject.getString("image"));

                listaPersonajes.add(personajes);

            }

            AdaptadorPersonajes adaptadorPersonajes = new AdaptadorPersonajes(listaPersonajes);
            rvPersonajes.setAdapter(adaptadorPersonajes);


            adaptadorPersonajes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Metodo Onclick
                    String nombre = listaPersonajes.get(rvPersonajes.getChildAdapterPosition(v)).getName();
                    Toast.makeText(getApplicationContext(), "Has seleccionado a " + nombre, Toast.LENGTH_SHORT).show();
                }
            });

        }catch (Exception e){

        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
    }

}
