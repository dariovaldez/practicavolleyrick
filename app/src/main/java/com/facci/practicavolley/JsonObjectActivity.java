package com.facci.practicavolley;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import org.json.JSONObject;

public class JsonObjectActivity extends AppCompatActivity
        implements Response.Listener<JSONObject>, Response.ErrorListener {

    Button btnBuscar;
    ImageView fotoPersonaje;
    TextView lbl_nombre, lbl_id, lbl_especie, lbl_genero;
    EditText txtID;

    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    ProgressBar pbCargar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_object);
        fotoPersonaje = (ImageView) findViewById(R.id.ivFotoPersonaje);
        lbl_nombre = (TextView) findViewById(R.id.lbl_nombre);
        lbl_id = (TextView) findViewById(R.id.lbl_id);
        lbl_especie = (TextView) findViewById(R.id.lbl_especie);
        lbl_genero = (TextView) findViewById(R.id.lbl_genero);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        txtID = (EditText) findViewById(R.id.txtIdPersonaje);
        pbCargar = (ProgressBar) findViewById(R.id.progressBar);

        requestQueue = Volley.newRequestQueue(this);

        pbCargar.setVisibility(View.INVISIBLE);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscarPersonaje();
            }
        });

    }

    private void buscarPersonaje() {
        String id = txtID.getText().toString();

        //Metodo que oculta el teclado
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtID.getWindowToken(), 0);

        if (!id.isEmpty()) {
            pbCargar.setVisibility(View.VISIBLE);
            String url = "https://rickandmortyapi.com/api/character/" + id;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            requestQueue.add(jsonObjectRequest);
        }else {
            txtID.setError("Campo Obligatorio");
            txtID.requestFocus();
        }

    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            Picasso.get().load(response.getString("image")).into(fotoPersonaje);
            lbl_id.setText("ID: " + response.getInt("id"));
            lbl_nombre.setText("Nombre: " + response.getString("name"));
            lbl_especie.setText("Especie: " + response.getString("species"));
            lbl_genero.setText("Genero: " + response.getString("gender"));
            pbCargar.setVisibility(View.INVISIBLE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al cargar personaje" + error, Toast.LENGTH_SHORT).show();
        pbCargar.setVisibility(View.INVISIBLE);
    }


}
