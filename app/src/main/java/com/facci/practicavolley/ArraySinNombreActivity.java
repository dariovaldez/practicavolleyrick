package com.facci.practicavolley;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.facci.practicavolley.Adaptador.AdaptadorUsuariosGitHub;
import com.facci.practicavolley.Modelo.UsuariosGithub;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ArraySinNombreActivity extends AppCompatActivity
                implements Response.Listener<JSONArray>, Response.ErrorListener {

    RecyclerView rvUsuarios;
    ArrayList<UsuariosGithub> listaUsuarios;

    RequestQueue request;
    JsonArrayRequest jsonArrayRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_array_sin_nombre);

        rvUsuarios = (RecyclerView) findViewById(R.id.recyclerUsuarios);
        listaUsuarios =new ArrayList<>();

        rvUsuarios.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));
        rvUsuarios.setHasFixedSize(true);

        request = Volley.newRequestQueue(getApplicationContext());

        cargarApi();



    }

    private void cargarApi() {
        String url = "https://api.github.com/users/jsinix/followers";
        jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonArrayRequest);

    }


    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONArray response) {
        try {
            for (int i = 0; i<response.length(); i++){
                UsuariosGithub usuario = new UsuariosGithub();
                JSONObject jsonObject = response.getJSONObject(i);
                usuario.setId(jsonObject.getInt("id"));
                usuario.setLogin(jsonObject.getString("login"));
                usuario.setAvatar_url(jsonObject.getString("avatar_url"));

                listaUsuarios.add(usuario);
            }
            AdaptadorUsuariosGitHub adaptadorUsuariosGitHub = new AdaptadorUsuariosGitHub(listaUsuarios);
            rvUsuarios.setAdapter(adaptadorUsuariosGitHub);

            adaptadorUsuariosGitHub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = listaUsuarios.get(rvUsuarios.getChildAdapterPosition(v)).getId();
                    String nombre = listaUsuarios.get(rvUsuarios.getChildAdapterPosition(v)).getLogin();
                    String urlImagen = listaUsuarios.get(rvUsuarios.getChildAdapterPosition(v)).getAvatar_url();
                    Intent intent = new Intent(getApplicationContext(), ItemClickUsuariosActivity.class);
                    intent.putExtra("id", id);
                    intent.putExtra("nombre", nombre);
                    intent.putExtra("urlImagen", urlImagen);
                    startActivity(intent);
                }
            });

        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "No se pudo cargar", Toast.LENGTH_SHORT).show();
        }
    }
}
