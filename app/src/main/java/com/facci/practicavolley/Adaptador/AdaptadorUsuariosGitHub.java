package com.facci.practicavolley.Adaptador;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facci.practicavolley.Modelo.UsuariosGithub;
import com.facci.practicavolley.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdaptadorUsuariosGitHub
        extends RecyclerView.Adapter<AdaptadorUsuariosGitHub.UsuariosViewHolder>
                implements View.OnClickListener {

    List<UsuariosGithub> listaUsuarios;
    private View.OnClickListener listener;

    public AdaptadorUsuariosGitHub(List<UsuariosGithub> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    @NonNull
    @Override
    public UsuariosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_usuario, parent, false);
        view.setOnClickListener(this);
        return new UsuariosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UsuariosViewHolder holder, int position) {
        //Los TextViews aceptan valores String,
        //al recibir un entero hay que parsearlo con StringValueOf para que se muestre el entero
        //caso contrario daria error y se cerrara la aplicacion
        holder.tv_id.setText(String.valueOf(listaUsuarios.get(position).getId()));
        holder.tv_nombreUsuario.setText(listaUsuarios.get(position).getLogin());
        Picasso.get().load(listaUsuarios.get(position).getAvatar_url()).into(holder.ivFotoUsuario);
    }

    @Override
    public int getItemCount() {
        return listaUsuarios.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null){
            listener.onClick(v);
        }
    }

    public class UsuariosViewHolder extends RecyclerView.ViewHolder {

        ImageView ivFotoUsuario;
        TextView tv_nombreUsuario, tv_id;

        public UsuariosViewHolder(@NonNull View itemView) {
            super(itemView);
            ivFotoUsuario = (ImageView) itemView.findViewById(R.id.iv_usuario);
            tv_nombreUsuario = (TextView) itemView.findViewById(R.id.tv_login);
            tv_id = (TextView) itemView.findViewById(R.id.tv_id);
        }
    }
}
