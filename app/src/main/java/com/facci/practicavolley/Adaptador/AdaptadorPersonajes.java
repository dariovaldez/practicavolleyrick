package com.facci.practicavolley.Adaptador;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facci.practicavolley.Modelo.Personajes;
import com.facci.practicavolley.R;
import com.squareup.picasso.Picasso;
import java.util.List;

public class AdaptadorPersonajes
        extends RecyclerView.Adapter<AdaptadorPersonajes.PersonajesViewHolder>
            implements View.OnClickListener {

    List<Personajes> listaPersonajes;

    public AdaptadorPersonajes(List<Personajes> listaPersonajes) {
        this.listaPersonajes = listaPersonajes;
    }

    private View.OnClickListener listener;



    @NonNull
    @Override
    public PersonajesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_personaje, parent, false);
        view.setOnClickListener(this);
        return new PersonajesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonajesViewHolder holder, int position) {
        holder.tv_nombre.setText(listaPersonajes.get(position).getName());
        holder.tv_genero.setText(listaPersonajes.get(position).getGender());
        Picasso.get().load(listaPersonajes.get(position).getImage()).into(holder.ivFoto);
    }

    @Override
    public int getItemCount() {
        return listaPersonajes.size();
    }

    public void setOnClickListener(View.OnClickListener listener){ this.listener = listener;
    }


    @Override
    public void onClick(View v) {
        if (listener != null){
            listener.onClick(v);
        }
    }

    public class PersonajesViewHolder extends RecyclerView.ViewHolder {

        ImageView ivFoto;
        TextView tv_nombre, tv_genero;

        public PersonajesViewHolder(@NonNull View itemView) {
            super(itemView);
            ivFoto = (ImageView) itemView.findViewById(R.id.iv_personaje);
            tv_nombre = (TextView) itemView.findViewById(R.id.tv_nombre);
            tv_genero = (TextView) itemView.findViewById(R.id.tv_genero);
        }
    }
}
