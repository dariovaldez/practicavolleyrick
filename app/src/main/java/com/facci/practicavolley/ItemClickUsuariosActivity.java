package com.facci.practicavolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.facci.practicavolley.R;
import com.squareup.picasso.Picasso;

public class ItemClickUsuariosActivity extends AppCompatActivity {

    ImageView ivFotoUsuario;
    TextView tv_nombre, tv_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_click_usuarios);
        ivFotoUsuario = (ImageView) findViewById(R.id.ivFotoUsuario);
        tv_id = (TextView) findViewById(R.id.lbl_idUsuario);
        tv_nombre = (TextView) findViewById(R.id.lbl_nombreUsuario);

        int id = getIntent().getIntExtra("id", 0);
        String nombre = getIntent().getStringExtra("nombre");
        String imagen = getIntent().getStringExtra("urlImagen");

        tv_id.setText("ID: " + id);
        tv_nombre.setText("Usuario: " + nombre);
        Picasso.get().load(imagen).into(ivFotoUsuario);

    }
}
